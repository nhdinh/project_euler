package euler.obstacle11;

import static org.junit.Assert.*;

import org.junit.Test;

public class Obstacle11_Test {

	@Test
	public void test() {
		Obstacle11 obstacle = new Obstacle11();

		long maxNumber = obstacle.Run();
		assertEquals(70600674, maxNumber);
	}
}
